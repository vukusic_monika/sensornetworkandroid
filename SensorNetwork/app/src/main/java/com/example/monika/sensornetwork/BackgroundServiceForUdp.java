package com.example.monika.sensornetwork;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Matija on 6/6/2017.
 */

public class BackgroundServiceForUdp extends Service {

    private JSONObject JObject;
    private Context appContext;

    //lista koju punimo s podacima za notification history activity
    private String[] listOfNotificationsNames = new String[100];
    private JSONArray[] listOfNotifications = new JSONArray[100];
    private int notificationCounter = -1;

    int UDP_SERVER = (UdpConfiguration.getAppConfiguration()).getUDPServer();
    String ipAdress = (UdpConfiguration.getAppConfiguration()).getIPAdress();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    boolean run = true;


    public class ClientListen implements Runnable {

        DatagramSocket udpSocket=null;

        ClientListen(DatagramSocket socket){
            this.udpSocket = socket;
        }

        @Override
        public void run() {

            while (run) {
                try {

                    byte[] message = new byte[8000];
                    DatagramPacket packet = new DatagramPacket(message,message.length);

                    this.udpSocket.receive(packet);
                    //udpSocket.setSoTimeout(5000);
                    String response = new String(message, 0, packet.getLength());
                    JObject = new JSONObject(response);
                    String josnResponse="";
                    Log.d("UDP", "in receive function");
                    if (JObject.has("condition")){
                        josnResponse = JObject.getString("condition");

                        if (josnResponse.equals("True")){
                            Log.d("UDP", "in receive function, condition je true");
                            JSONArray jsonObject = JObject.getJSONArray("notifications");
                            //punimo listu notifikacija, kada se dosegne max lista se ponovno inicijalizira
                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                            notificationCounter ++;

                            if (notificationCounter == 99) {
                                listOfNotificationsNames=new String[100];
                                listOfNotifications = new JSONArray[100];
                                notificationCounter = 0;
                                listOfNotificationsNames[notificationCounter] = "Data received at: " + currentDateTimeString;
                                listOfNotifications[notificationCounter] = jsonObject;
                            }
                            else {
                                listOfNotificationsNames[notificationCounter] = "Data received at: " + currentDateTimeString;
                                listOfNotifications[notificationCounter] = jsonObject;
                            }

                            DataHolder.setData(listOfNotificationsNames);
                            DataHolder.setNotificationObjects(listOfNotifications);
                            DataHolder.setLastIndex(notificationCounter);

                            CallNotificationIntent not = new CallNotificationIntent(jsonObject, getApplicationContext());
                        }
                    }
                    else if(JObject.has("success")){
                        josnResponse = JObject.getString("success");
                        if (josnResponse.equals("True")) {
                            Log.d("UDP-suces", "Podaci dodani na UDP" + DataHolder.getUdpMessage());
                            DataHolder.setUdpMessage("{'type':'notification check'}");
                        }
                    }

                } catch (SocketException e) {
                    Log.e("UDP", "Socket Error:", e);
                    //run = false;
                }
                catch (IOException e) {
                    Log.d("UDP", "error: ", e);
                    //run = false;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class ClientSend implements Runnable {

        DatagramSocket udpSocket=null;
        ClientSend(DatagramSocket socket){
            this.udpSocket = socket;
        }

        @Override
        public void run() {

            while(run){

                try {
                    InetAddress serverAddr = InetAddress.getByName(ipAdress);

                    String str = "{'type':'notification check'}";
                    str = DataHolder.getUdpMessage();
                    Log.d("UDPmsg", str);
                    JSONObject tempObj=null;
                    try {
                        tempObj = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //Log.d("UDP", tempObj.toString());

                    String sendData = tempObj.toString();
                    byte[] buf = sendData.getBytes();
                    DatagramPacket packet = new DatagramPacket(buf, buf.length,serverAddr, UDP_SERVER);
                    Log.d("UDP", "in send function");
                    this.udpSocket.send(packet);
                    Log.d("UDP", "in send function, after socket send");
                    //udpSocket.setSoTimeout(5000);
                }catch (SocketException e) {
                    Log.e("UDP", "Socket Error:", e);
                    //run = false;
                }catch (IOException e) {
                    Log.e("UDP:", "IO Error:", e);
                    //run = false;
                }

                SystemClock.sleep(5000);

            }

        }
    }

    @Override
    public void onCreate() {

    };

    @Override
    public void onDestroy() {
        run = false;
        Intent broadcastIntent = new Intent("com.example.monika.sensornetwork.RestartSensor");
        sendBroadcast(broadcastIntent);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("UDP", "Service started");
        run = true;
        DatagramSocket udpSocket = null;
        appContext = getBaseContext();

        try{

            udpSocket = new DatagramSocket();

        }catch(SocketException e){

            e.printStackTrace();

        }

        ClientListen clientListen = new ClientListen(udpSocket);
        new Thread(clientListen).start();

        ClientSend clientSend = new ClientSend(udpSocket);
        new Thread(clientSend).start();

        Log.d("UDP", "Run Thread");
        return START_STICKY;
    }
}
