package com.example.monika.sensornetwork;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Monika on 6.6.2017..
 */

public class PatchExample extends AsyncTask<String, Void, String> {

    private String id = null;
    private final Activity activity;
    private final String json;
    private String returnValue = "", thingObjectStr;
    private JSONObject locations, thingObject, sensorObject;
    private String thing, site="displayThing";
    private JSONArray datastreamsArray;
    private String entity="";
    private JSONObject patchedLocation;


    PatchExample(Activity activity, String id, JSONObject thing) throws JSONException {
        this.id = id;
        JSONObject jsonThing = thing;
        this.activity = activity;
        this.json = jsonThing.toString();
    }

    PatchExample(Activity activity, String id, JSONObject thing, String entity) throws JSONException {
        this.id = id;
        this.entity = entity;
        JSONObject jsonThing = thing;
        this.activity = activity;
        this.json = jsonThing.toString();
    }

    PatchExample(Activity activity, String id, JSONObject thing, String site, JSONObject sensor, JSONArray datastreams) throws JSONException {
        this.id = id;
        JSONObject jsonThing = thing;
        this.activity = activity;
        this.site = site;
        this.json = jsonThing.toString();
        this.thingObject = thing;
        this.sensorObject = sensor;
        this.datastreamsArray = datastreams;
    }

    /*PatchExample(Activity activity, String id, JSONObject thing, String site, JSONObject sensor, JSONArray datastreams) throws JSONException {
        this.id = id;
        JSONObject jsonThing = thing;
        this.activity = activity;
        this.site = site;
        this.json = jsonThing.toString();
        this.thingObject = thing;
        this.sensorObject = sensor;
        this.datastreamsArray = datastreams;
    }*/

    /*PatchExample(Activity activity, String id, JSONObject thing, String site) throws JSONException {
        this.id = id;
        JSONObject jsonThing = thing;
        this.activity = activity;
        this.site = site;
        this.json = jsonThing.toString();
    }*/


    OkHttpClient client = new OkHttpClient();

    private static final MediaType JSON  = MediaType.parse("application/json;charset=UTF-8");

    String doPostRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);


        String credential = Credentials.basic("academic", "8b59150a");

        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .addHeader("Authorization", credential)
                .url(url)
                .patch(body)
                .build();
        Response response = client.newCall(request).execute();

        return response.body().string();
    }

    String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    protected String doInBackground(String... params) {
        if (this.entity.equals("patchLocation")) {
            String patchedLoc = null;
            try {
                patchedLoc = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations(" + this.id + ")", json);
                this.patchedLocation = new JSONObject(patchedLoc);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                this.returnValue = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things(" + this.id + ")", json);

                this.thing = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things(" + this.id + ")?$expand=Locations");

                this.thingObjectStr = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things(" + this.id + ")");

                String returnValueLocation = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations");
                this.locations = new JSONObject(returnValueLocation);

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    @Override
    protected void onPreExecute() {}

    @Override
    protected void onProgressUpdate(Void... values) {}

    @Override
    protected void onPostExecute(String result) {

        if (this.entity.equals("patchLocation")) {
            onPostExecutePatchLoc();
        }
        else if (site.equals("displaySensor")) {

            Log.d("UDP", "Patch data: " + this.returnValue);

            Intent intent = new Intent(activity, DisplaySensorTabActivity.class);

            intent.putExtra("datastream", datastreamsArray.toString());
            intent.putExtra("sensor", sensorObject.toString());

            try {
                JSONObject newThing = new JSONObject(this.thing);
                Log.d("NE_RADI1", newThing.toString());
                intent.putExtra("thing", newThing.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra("tabToOpen", 1);

            int duration = Toast.LENGTH_LONG;
            Toast.makeText(activity, "Conditions succesfully edited.", duration).show();

            activity.startActivity(intent);
            this.activity.finish();
        }
        else if (site.equals("displayThing")){
            Log.d("post", this.returnValue);
            Intent intent = new Intent(activity, DisplayThingTabActivity.class);
            intent.putExtra("things", this.thing);
            intent.putExtra("locations", this.locations.toString());
            intent.putExtra("tabToOpen", 1);
            activity.startActivity(intent);

            Log.d("DisplaySensorTab", "thing");

            int duration = Toast.LENGTH_LONG;
            Toast.makeText(activity, "Succesfully edited.", duration).show();
            this.activity.finish();

        }

        }

    private void onPostExecutePatchLoc() {
        int duration = Toast.LENGTH_LONG;
        Toast.makeText(this.activity, "Sucessfuly changed location info.", duration).show();

        final Intent intent = new Intent(activity, DisplayLocationTab.class);
        intent.putExtra("location", this.patchedLocation.toString());

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                    activity.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();

    }
}


