package com.example.monika.sensornetwork;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;

public class NotificationHistory extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<String> listOfNotifications=null;
    JSONArray[] notifications;
    int notificationCount = -1;
    FloatingActionButton fabHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_history);
        ListView lv = (ListView) findViewById(R.id.notificationHistory);
        TextView tv = (TextView) findViewById(R.id.textViewNoRecent);

        fabHistory = (FloatingActionButton)findViewById(R.id.refresh);
        fabHistory.setOnClickListener(this);

        String[] notificationNames = DataHolder.getData();
        notifications = DataHolder.getNotificationObjects();


        notificationCount = DataHolder.getLastIndex();

        if (notificationCount != -1){

            lv.setVisibility(View.VISIBLE);
            tv.setVisibility(View.GONE);

            listOfNotifications = new ArrayList<String>(Integer.parseInt(String.valueOf(notificationCount)));

            for (int i=0; i<=notificationCount; i++){

                listOfNotifications.add(notificationNames[i]);

            }

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listOfNotifications);

            lv.setAdapter(adapter);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(getApplicationContext(), NotificationResultActivity.class);
                intent.putExtra("sensors", notifications[position].toString());
                startActivity(intent);



                }
            });

        }else{

            lv.setVisibility(View.GONE);
            tv.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id){
            case R.id.refresh:
                Intent intent = getIntent();
                finish();
                startActivity(intent);
        }

    }
}
