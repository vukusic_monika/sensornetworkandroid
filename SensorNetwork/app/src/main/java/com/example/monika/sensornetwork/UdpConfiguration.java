package com.example.monika.sensornetwork;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Created by Matija on 7/2/2017.
 */

public class UdpConfiguration extends Application {

    private int UdpServer = 9998;
    private String IpAdress = "192.168.43.191";
    private static UdpConfiguration instance;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public int getUDPServer() {
        return UdpServer;
    }

    public void setUDPServer(int portNumber) {
        this.UdpServer = portNumber;
    }

    public String getIPAdress() {
        return IpAdress;
    }

    public void setIPAdress(String ipAdress) {
        this.IpAdress = ipAdress;
    }

    public static UdpConfiguration getAppConfiguration() {
        return instance;
    }
}
