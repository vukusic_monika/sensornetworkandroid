package com.example.monika.sensornetwork;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Matija on 6/6/2017.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ( "android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) || "android.intent.action.QUICKBOOT_POWERON".equals(intent.getAction()) ) {
            Intent serviceIntent = new Intent("com.example.monika.sensornetwork.BackgroundServiceForUdp");
            context.startService(serviceIntent);
        }
    }
}
