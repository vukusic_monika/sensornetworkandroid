package com.example.monika.sensornetwork;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by Monika on 25.5.2017..
 */

public class Tab1Info extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab1info, container, false);

        Bundle bundle=getArguments();
        if(bundle!=null) {

            if (bundle.containsKey("things")) {
                try {
                    JSONObject jsonObj = new JSONObject(bundle.getString("things"));

                    TextView iotIdValue = (TextView) rootView.findViewById(R.id.textIotIdValue);
                    TextView iotSelfLinkValue = (TextView) rootView.findViewById(R.id.textIotSelfLinkValue);
                    TextView iotDescriptionValue = (TextView) rootView.findViewById(R.id.textIotDescriptionValue);
                    TextView iotNameValue = (TextView) rootView.findViewById(R.id.textThingIotNameValue);

                    iotIdValue.setText(String.valueOf(jsonObj.getInt("@iot.id")));

                    String selfLink = jsonObj.getString("@iot.selfLink");
                    selfLink = selfLink.substring(0, 38) + "display/" + selfLink.substring(38, selfLink.length());
                    iotSelfLinkValue.setText(selfLink);

                    iotDescriptionValue.setText(jsonObj.getString("description"));
                    iotNameValue.setText(jsonObj.getString("name"));

                    JSONObject properties = jsonObj.getJSONObject("properties");
                    TableLayout tl = (TableLayout) rootView.findViewById(R.id.thingInfoTable);

                    Iterator<String> temp = properties.keys();
                    if (properties.length() <= 0) {
                        TableRow row = new TableRow(getActivity());
                        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                        row.setLayoutParams(lp);
                        TextView sensor = new TextView(getActivity());
                        String text = "No connected sensors.";
                        sensor.setText(text);
                        row.addView(sensor);
                        tl.addView(row);
                    } else {
                        while (temp.hasNext()) {
                            String key = temp.next();
                            JSONObject value = (JSONObject) properties.get(key);

                            TableRow row = new TableRow(getActivity());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                            row.setLayoutParams(lp);
                            TextView sensor = new TextView(getActivity());
                            sensor.setText(key);
                            //sensor.setTypeface(null, Typeface.BOLD);
                            row.addView(sensor);
                            tl.addView(row);

                        /*Iterator<String> temp2 = value.keys();
                        while (temp2.hasNext()) {
                            String key2 = temp2.next();
                            Object value2 = value.get(key2);

                            Log.d("Tab1Info", "key" + key2);
                            Log.d("Tab1Info", "value" + value2.toString());

                            if (key2.equals("condition") && value2 instanceof JSONObject) {
                                Log.d("Tab1Info", "value2" + value2.toString());
                                JSONObject datastreamsPairs = (JSONObject) value2;
                                Iterator<String> temp3 = datastreamsPairs.keys();

                                TableRow row2 = new TableRow(getActivity());
                                row2.setLayoutParams(lp);
                                TextView tv= new TextView(getActivity());
                                tv.setText(key2);
                                row2.addView(tv);
                                tl.addView(row2);

                                while(temp3.hasNext()) {
                                    String key3 = temp3.next();
                                    Object value3 = datastreamsPairs.get(key3);

                                    TableRow row3 = new TableRow(getActivity());
                                    row3.setLayoutParams(lp);
                                    TextView propertiesValue = new TextView(getActivity());
                                    propertiesValue.setText("datastream " + key3 + ": " + value3);
                                    row3.addView(propertiesValue);
                                    tl.addView(row3);
                                }
                            } else {
                                TableRow row2 = new TableRow(getActivity());
                                row.setLayoutParams(lp);
                                TextView propertiesValue = new TextView(getActivity());
                                propertiesValue.setText(key2 + ": " + value2);
                                row2.addView(propertiesValue);
                                tl.addView(row2);
                            }
                        }*/
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return rootView;
    }
}
