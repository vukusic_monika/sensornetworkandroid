package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

public class DisplaySensorThing extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_sensor_thing);

        Bundle extras = getIntent().getExtras();
        JSONObject jsonObj = null;

        if (extras != null) {
            try {
                jsonObj = new JSONObject(extras.getString("sensor"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ListView lv = (ListView) findViewById(R.id.sensorThingView);

        GetExample example = null;
        try {
            example = new GetExample(lv, this, "sensorThing", jsonObj.getString("@iot.selfLink"));
            example.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
