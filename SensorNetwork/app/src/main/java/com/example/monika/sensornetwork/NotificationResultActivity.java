package com.example.monika.sensornetwork;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class NotificationResultActivity extends AppCompatActivity implements View.OnClickListener {

    private JSONArray reveivedData;
    String resultData="", thingDataStr="", sensorDataStr="", datastreamsArray="";
    String[] tempArrayDatastreams, tempArraySensors, tempArrayThings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_result);

        Bundle extras = getIntent().getExtras();
        String sensorData = extras.getString("sensors");
        TableLayout tableLayout = (TableLayout)findViewById(R.id.layoutId);

        try {

            reveivedData = new JSONArray(sensorData);
            tempArrayDatastreams = new String[reveivedData.length()];
            tempArraySensors = new String[reveivedData.length()];
            tempArrayThings = new String[reveivedData.length()];

            for(int i = 0; i < reveivedData.length(); i++)
            {

                JSONObject element = reveivedData.getJSONObject(i);
                String datastreamID = element.getString("datastreamId");
                String measuredValue = element.getString("data");

                GetExample example = new GetExample("Datastreams", datastreamID);
                try {
                    resultData = example.execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                JSONObject datastreamData = null;
                datastreamData = new JSONObject(resultData);

                String linkForThing = datastreamData.getString("Thing@iot.navigationLink");
                String linkForSensor = datastreamData.getString("Sensor@iot.navigationLink");

                GetExample thingData = new GetExample(linkForThing);
                GetExample sensorDataGet = new GetExample(linkForSensor);
                try {
                    thingDataStr = thingData.execute().get();
                    sensorDataStr = sensorDataGet.execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                JSONObject sensorDataJson = null;
                sensorDataJson = new JSONObject(sensorDataStr);
                String linkForDatastreansArray = sensorDataJson.getString("Datastreams@iot.navigationLink");

                JSONObject thingObj = new JSONObject(thingDataStr);
                String thingName = thingObj.getString("description");

                GetExample datastreamDataGet = new GetExample(linkForDatastreansArray);
                try {
                    datastreamsArray = datastreamDataGet.execute().get();
                    JSONObject jsonObj = new JSONObject(datastreamsArray);
                    datastreamsArray = jsonObj.getString("value");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


                final TableRow tableRow = new TableRow(this);
                final TextView textView = new TextView(this);
                textView.setTextSize(20);
                textView.setTextColor(getResources().getColor(R.color.colorPrimary));
                textView.setText("Notification data: ");
                tableRow.addView(textView);

                final TableRow tableRow1 = new TableRow(this);
                final TextView editText1 = new TextView(this);
                editText1.setText("Description:");
                editText1.setTypeface(null, Typeface.BOLD);
                tableRow1.addView(editText1);

                final TableRow tableRow2 = new TableRow(this);
                final TextView editText2 = new TextView(this);
                editText2.setText(datastreamData.getString("description"));
                editText2.append("\n");
                tableRow2.addView(editText2);

                final TableRow tableRow3 = new TableRow(this);
                final TextView editText3 = new TextView(this);
                editText3.setText("Sensor name:");
                editText3.setTypeface(null, Typeface.BOLD);
                tableRow3.addView(editText3);

                final TableRow tableRow4 = new TableRow(this);
                final TextView editText4 = new TextView(this);
                editText4.setText(datastreamData.getString("name"));
                editText4.append("\n");
                tableRow4.addView(editText4);

                final TableRow tableRow5 = new TableRow(this);
                final TextView editText5 = new TextView(this);
                editText5.setText("Measured value: ");
                editText5.setTypeface(null, Typeface.BOLD);
                tableRow5.addView(editText5);

                final TableRow tableRow6 = new TableRow(this);
                final TextView editText6 = new TextView(this);
                editText6.append(measuredValue);
                JSONObject tempObj = datastreamData.getJSONObject("unitOfMeasurement");
                String mjernaJedinica = tempObj.getString("symbol");
                if (mjernaJedinica.equals("none")) mjernaJedinica = "";
                editText6.append(" "+mjernaJedinica);
                editText6.append("\n");
                tableRow6.addView(editText6);

                final TableRow tableRow7 = new TableRow(this);
                final Button editText7 = new Button(this);
                editText7.setText("SENSOR FULL VIEW");
                //editText7.setLayoutParams(layoutParams);
                //editText7.setTypeface(null, Typeface.BOLD);
                editText7.setOnClickListener(this);
                tableRow7.addView(editText7);
                editText7.setId(i);

                tempArrayDatastreams[i] = datastreamsArray;
                tempArraySensors[i] = sensorDataStr;
                tempArrayThings[i] = thingDataStr;

                final TableRow tableRow8 = new TableRow(this);
                final TextView editText8 = new TextView(this);
                editText8.setText("Device: ");
                editText8.setTypeface(null, Typeface.BOLD);
                tableRow8.addView(editText8);

                final TableRow tableRow9 = new TableRow(this);
                final TextView editText9 = new TextView(this);
                //editText9.setText(datastreamData.getString("Thing@iot.navigationLink"));
                editText9.setText(thingName);
                editText9.append("\n");
                tableRow9.addView(editText9);

                tableLayout.addView(tableRow);
                tableLayout.addView(tableRow3);
                tableLayout.addView(tableRow4);
                tableLayout.addView(tableRow1);
                tableLayout.addView(tableRow2);
                tableLayout.addView(tableRow5);
                tableLayout.addView(tableRow6);
                tableLayout.addView(tableRow7);
                tableLayout.addView(tableRow8);
                tableLayout.addView(tableRow9);



            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(getApplication(), DisplaySensorTabActivity.class);
        intent.putExtra("datastream", tempArrayDatastreams[v.getId()]);
        intent.putExtra("sensor", tempArraySensors[v.getId()]);
        intent.putExtra("thing", tempArrayThings[v.getId()]);
        intent.putExtra("tabToOpen", 0);

        startActivity(intent);

    }
}
