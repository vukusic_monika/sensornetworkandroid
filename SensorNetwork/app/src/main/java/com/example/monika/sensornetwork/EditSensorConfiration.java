package com.example.monika.sensornetwork;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class EditSensorConfiration extends AppCompatActivity implements View.OnClickListener {

    private JSONObject sensorObject, thingObject, properties;
    private JSONArray datastreamArray;
    private String datastreamId="", sensorType="",thingId="", datastreamCondition="";
    private int sensorID = -1;
    Button buttonCondition, buttonRemove;
    //TextView senState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sensor_confiration);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null) {
            try {

                sensorObject = new JSONObject(bundle.getString("sensor"));
                datastreamArray = new JSONArray(bundle.getString("datastream"));
                thingObject = new JSONObject(bundle.getString("thing"));
                Log.d("NE_RADI_Edit1", thingObject.toString());
                properties = new JSONObject(bundle.getString("properties"));
                datastreamId = bundle.getString("datastreamId");


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        setView();

    }


    protected void setView(){

        JSONObject sensorCon;

        try {

            sensorID = sensorObject.getInt("@iot.id");

            if (sensorID == 4) sensorType = "DHT11";
            else if (sensorID == 1594) sensorType = "BISS0001";
            else if (sensorID == 5122) sensorType = "Bluetooth";

            thingId = thingObject.getString("@iot.id");
            String tmp = properties.getString(sensorType);;
            JSONObject datastreamData = new JSONObject(tmp);
            tmp = datastreamData.getString("condition");
            sensorCon = new JSONObject(tmp);
            datastreamCondition = sensorCon.getString(datastreamId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView notificationCondition = (TextView) findViewById(R.id.changeCondValue);
        notificationCondition.setText("Conditions for this sensor: " + datastreamCondition);

        buttonCondition = (Button) findViewById(R.id.buttonChangeCondition);
        buttonCondition.setOnClickListener(this);

        //buttonState = (Button) findViewById(R.id.buttonTurnOnOff);
        //buttonState.setOnClickListener(this);

        buttonRemove = (Button) findViewById(R.id.buttonRemoveCondition);
        buttonRemove.setOnClickListener(this);

        /*senState = (TextView) findViewById(R.id.sensorOnValue);

        if (sensorState.equals("True")) {
            buttonState.setText("Turn off");
            senState.setText("Sensor is on");
        } else {
            buttonState.setText("Turn on");
            senState.setText("Sensor is off");
        }*/

    }

    @Override
    public void onClick(View v) {

        JSONObject thing, sendProperties;
        PatchExample server;
        String successResponse="";

        //pritisnut button za izmjenu konfiguracije
        if (v.getId() == buttonCondition.getId()) {

            Log.d("UDP", "button condition configuration");
            if (sensorType.equals("BISS0001")){

                Intent intent = new Intent(this, ChangeSensorConditionValue.class);
                intent.putExtra("properties", properties.toString());
                intent.putExtra("sensor", sensorObject.toString());
                intent.putExtra("datastream", datastreamArray.toString());
                intent.putExtra("thing", thingObject.toString());
                intent.putExtra("id", thingId);
                intent.putExtra("type", sensorType);
                intent.putExtra("datastreamId", datastreamId);
                intent.putExtra("datastreamCondition", datastreamCondition);
                startActivity(intent);
                this.finish();
            }else{

                Intent intent = new Intent(this, ChangeSensorConditionSpan.class);
                intent.putExtra("properties", properties.toString());
                intent.putExtra("id", thingId);
                intent.putExtra("type", sensorType);
                intent.putExtra("thing", thingObject.toString());
                Log.d("NE_RADI_ESC", thingObject.toString());
                intent.putExtra("datastream", datastreamArray.toString());
                intent.putExtra("sensor", sensorObject.toString());
                intent.putExtra("datastreamCondition", datastreamCondition);
                intent.putExtra("datastreamId", datastreamId);
                startActivity(intent);
                this.finish();
            }

        }

        //pritisnut button za palit/gasit senzor
        /*else if (v.getId() == buttonState.getId()) {

            Log.d("UDP", "button on/off configuration");
            String UDPmsg="", newSensorState="";
            if (sensorState.equals("True")) {
                UDPmsg = "{'type':'sensorOnOff','datastreamId':" + datastreamId + ", 'sensorOn':'False'}";
                newSensorState = "False";
            }else {
                UDPmsg = "{'type':'sensorOnOff','datastreamId':" + datastreamId + ", 'sensorOn':'True'}";
                newSensorState = "True";
            }

            //kad klijent izmjeneni stanje senzora salje na python server
            //ako server vrati success true postaj to na serverUp server
            Log.d("UDP_S", "Pripremam podatke za slanje na UDP server...");
            ClientUdpSender cilentSender = new ClientUdpSender(UDPmsg);

            try {
                String response = cilentSender.execute().get();
                JSONObject jsonObj = new JSONObject(response);
                if (jsonObj.has("success")) {

                    successResponse = jsonObj.getString("success");
                    if (successResponse.equals("True")) {
                        Log.d("UDP", "Data updated on server UDP");
                        Log.d("UDP", "Sending on sensorUp...");
                        thing = new JSONObject();
                        sendProperties = prepareData(properties, sensorType, "sensorOn", newSensorState, false);
                        Log.d("UDP-data to send", sendProperties.toString());
                        thing.put("properties", sendProperties);
                        server = new PatchExample(this, thingId, thing, "displaySensor", sensorObject, datastreamArray);
                        server.execute();

                    } else {

                        Log.d("UDP_S", "Success key is set to false");
                        CharSequence text = "Data update failed on server UDP";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(this, text, duration);
                        toast.show();
                    }

                }else{
                    Log.d("UDP", "No success key in response");
                    CharSequence text = "Data update failed on server UDP";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(this, text, duration);
                    toast.show();
                }

            } catch (InterruptedException | JSONException | ExecutionException e) {
                e.printStackTrace();
            }

        }*/

        //kada korisnik pritisne remove condition button
        else if (v.getId() == buttonRemove.getId()) {

            Log.d("UDP", "remove button");
            String msg = "{'type':'change conditions', 'datastreamId':" + datastreamId + ", 'condition':'none'}";

            Log.d("UDP_S", "Pripremam podatke za slanje na UDP server...");
            ClientUdpSender cilentSender = new ClientUdpSender(msg);
            try {
                String response = cilentSender.execute().get();
                JSONObject jsonObj = new JSONObject(response);
                if (jsonObj.has("success")) {

                    successResponse = jsonObj.getString("success");
                    if (successResponse.equals("True")) {

                        Log.d("UDP", "Podaci spremljeni na UDP server");
                        Log.d("UDP", "Pripremam podatke za SensorUp server...");

                        sendProperties = prepareData(properties, sensorType, "condition", "none");
                        Log.d("TEST", sendProperties.toString());
                        try {
                            thing = new JSONObject();
                            thing.put("properties", sendProperties);
                            server = new PatchExample(this, thingId, thing, "displaySensor", sensorObject, datastreamArray);
                            server.execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{

                        Log.d("UDP_S", "Success key is set to false");
                        CharSequence text = "Data update failed on server UDP";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(this, text, duration);
                        toast.show();

                    }

                }else{

                    Log.d("UDP_S", "No success key in response");
                    CharSequence text = "Data update failed on server UDP";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(this, text, duration);
                    toast.show();

                }

            } catch (InterruptedException | JSONException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }


    private JSONObject prepareData(JSONObject thingProperties, String type, String sensorKey, String state) throws JSONException {

        JSONObject conditionObj = thingProperties.getJSONObject(type);

        JSONObject datastreamObj = conditionObj.getJSONObject(sensorKey);
        datastreamObj.remove(String.valueOf(datastreamId));
        datastreamObj.put(String.valueOf(datastreamId), state);


        return thingProperties;

    }

}
