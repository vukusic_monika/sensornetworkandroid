package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.io.IOException;

public class DisplaySensorsActivity extends AppCompatActivity {

    public void loadSensors (View view) throws IOException {

        ListView lv = (ListView) findViewById(R.id.sensorsView);

        GetExample example = new GetExample(lv, this, "sensors");
        example.execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_sensors);

        ListView lv = (ListView) findViewById(R.id.sensorsView);

        GetExample example = new GetExample(lv, this, "sensors");
        example.execute();
    }
}
