package com.example.monika.sensornetwork;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Monika on 25.5.2017..
 */

public class Tab2Edit extends Fragment implements View.OnClickListener {

    Button btn;
    JSONObject jsonObj = null;
    JSONObject locationsObj = null;
    String thingId = null;
    EditText iotNameValue = null, iotDescriptionValue = null;
    String locationId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab2edit, container, false);

        Bundle bundle=getArguments();

        if(bundle!=null) {
            if (bundle.containsKey("things")) {
                try {
                    jsonObj = new JSONObject(bundle.getString("things"));
                    locationsObj = new JSONObject(bundle.getString("locations"));

                    final JSONArray locationArray = locationsObj.getJSONArray("value");
                    int count = Integer.parseInt(locationsObj.getString("@iot.count"));

                    JSONArray thingLocationsArray = jsonObj.getJSONArray("Locations");

                    Spinner dynamicSpinner = (Spinner) rootView.findViewById(R.id.textLocationValue);

                    String[] locationNames;
                    int defaultValue;
                    String thingLocationName = "";
                    if (thingLocationsArray.length() <= 0) {
                        locationNames = new String[count+1];
                        locationNames[count] = "No location";
                        defaultValue = count;
                    }
                    else {
                        locationNames = new String[count];
                        defaultValue = 0;
                        JSONObject thingLocObj = (JSONObject) thingLocationsArray.get(0);
                        thingLocationName = thingLocObj.getString("name");
                    }

                    for (int i = 0; i < count; i++) {
                        JSONObject location = locationArray.getJSONObject(i);
                        locationNames[i] = location.getString("name");

                        if (locationNames[i].equals(thingLocationName))
                            defaultValue = i;
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_item, locationNames);

                    dynamicSpinner.setAdapter(adapter);

                    dynamicSpinner.setSelection(defaultValue);

                    dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            Log.v("item", (String) parent.getItemAtPosition(position));
                            String selectedItem = (String) parent.getItemAtPosition(position);
                            try {
                                if ( selectedItem.equals("No location") ){
                                    locationId = "-1";
                                }
                                else {
                                    JSONObject selectedLocation = (JSONObject) locationArray.get(position);
                                    locationId = selectedLocation.getString("@iot.id");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                    iotNameValue = (EditText) rootView.findViewById(R.id.textThingIotNameChange);
                    iotDescriptionValue = (EditText) rootView.findViewById(R.id.textThingIotDescriptionChange);

                    thingId = jsonObj.getString("@iot.id");
                    iotNameValue.setText(String.valueOf(jsonObj.getString("name")));
                    iotDescriptionValue.setText(jsonObj.getString("description"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        btn = (Button) rootView.findViewById(R.id.buttonChangeThing);
        btn.setOnClickListener(this);

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {

        PatchExample server = null;

        String iotNameValueS = iotNameValue.getText().toString();
        String iotDescriptionValueS = iotDescriptionValue.getText().toString();

        if (iotNameValueS.isEmpty() || iotDescriptionValueS.isEmpty()) {
            CharSequence text = "Name and description are mandatory!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(getActivity(), text, duration);
            toast.show();
        } else {
            JSONObject thing = new JSONObject();
            try {
                thing.put("name", iotNameValueS);
                thing.put("description", iotDescriptionValueS);

                JSONArray locationArray = new JSONArray();
                JSONObject locationIdObj = new JSONObject();

                if (!String.valueOf(locationId).equals("-1")) {
                    locationIdObj.put("@iot.id", String.valueOf(locationId));
                    locationArray.put(locationIdObj);

                    thing.put("Locations", locationArray);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                server = new PatchExample(getActivity(), thingId, thing);
                server.execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
