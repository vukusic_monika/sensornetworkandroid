package com.example.monika.sensornetwork;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Monika on 25.5.2017..
 */

public class Tab3Add extends Fragment implements View.OnClickListener {

    Button btn;
    JSONObject jsonObj = null;
    String thingId = null;
    EditText iotNameValue = null, iotDescriptionValue = null, iotPropertiesValue = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab3add, container, false);

        iotNameValue = (EditText) rootView.findViewById(R.id.textThingIotNameNewChange);
        iotDescriptionValue = (EditText) rootView.findViewById(R.id.textThingIotDescriptionNewChange);
        iotPropertiesValue = (EditText) rootView.findViewById(R.id.textThingIotPropertiesNewChange);

        btn = (Button) rootView.findViewById(R.id.buttonChangeThingNew);
        btn.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        PostExample server = null;

        String iotNameValueS = iotNameValue.getText().toString();
        String iotDescriptionValueS = iotDescriptionValue.getText().toString();
        String iotPropertiesValueS = iotPropertiesValue.getText().toString();

        if (iotNameValueS.isEmpty() || iotDescriptionValueS.isEmpty()) {
            CharSequence text = "Name and description are mandatory!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(getActivity(), text, duration);
            toast.show();
        } else {

            JSONObject thing = new JSONObject();
            try {
                thing.put("name", iotNameValueS);
                thing.put("description", iotDescriptionValueS);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                server = new PostExample(getActivity(), thing);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            server.execute();
        }
    }
}
